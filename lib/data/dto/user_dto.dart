
class UserDTO{
  final String name;
  final double currentBalance;
  final double maxBalance;
  final String balanceCurrency;
  late final double progress;

  UserDTO(this.name,this.balanceCurrency,this.currentBalance,this.maxBalance){
    progress = currentBalance/maxBalance;
  }
}