
import 'package:flutter/material.dart';

class CardDTO {
  final String numberCard;
  final String currency;
  final double value;
  final double maxValue;
  final String urlIcon;
  final bool isOverdue;
  final Color color;
  double progress = 0.0;

  CardDTO(this.numberCard, this.currency, this.value, this.maxValue,
      this.urlIcon, this.color, this.isOverdue) {
    progress = value / maxValue;
  }

  static get MOCK => CardDTO(
        "**** 0478",
        r'£',
        500,
        100.0,
        '',
        Colors.deepPurple,
        false,
      );
}
