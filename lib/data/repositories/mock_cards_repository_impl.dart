import 'package:cardeo_test_task/data/dto/card_dto.dart';
import 'package:cardeo_test_task/domain/repositories/cards_repository.dart';

class MockCardsRepositoryImpl implements CardsRepository {
  @override
  List<CardDTO> fetch() => [
        CardDTO.MOCK,
        CardDTO.MOCK,
        CardDTO.MOCK,
        CardDTO.MOCK,
      ];
}
