import 'package:cardeo_test_task/data/dto/user_dto.dart';
import 'package:cardeo_test_task/domain/repositories/user_repository.dart';

class MockUserRepositoryImpl implements UserRepository {
  @override
  UserDTO fetch() => UserDTO('James', '£', 42.42, 100);
}
