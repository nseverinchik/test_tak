import 'package:cardeo_test_task/data/dto/card_dto.dart';
import 'package:cardeo_test_task/presentation/utils/Consts.dart';
import 'package:cardeo_test_task/presentation/utils/extensions/context_extension.dart';
import 'package:flutter/material.dart';

class ItemCard extends StatelessWidget {
  final CardDTO card;
  final double cardHeight;

  const ItemCard({required this.card, required this.cardHeight, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: cardHeight,
      child: Row(
        children: [
          const _IconItemCard(size: Constants.iconItemCardSize),
          Expanded(
            child: Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: context.resources.dimens.smallPadding),
                child: _ContentItemCard(
                  card: card,
                )),
          )
        ],
      ),
    );
  }
}

class _IconItemCard extends StatelessWidget {
  final double size;

  const _IconItemCard({required this.size, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: size,
      width: size,
      decoration: BoxDecoration(
          shape: BoxShape.circle, color: context.resources.colors.drover),
      child: Icon(Icons.credit_card_outlined,
          color: context.resources.colors.blueDianne),
    );
  }
}

class _ContentItemCard extends StatelessWidget {
  final CardDTO card;

  const _ContentItemCard({Key? key, required this.card}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          flex: 1,
          child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  card.numberCard,
                  style: TextStyle(
                      fontSize: context.resources.dimens.smallTextSize,
                      color: Colors.grey),
                  textAlign: TextAlign.start,
                ),
                Text(
                  '${card.currency}${card.value}',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: context.resources.dimens.mediumTextSize,
                      color: context.resources.colors.blueDianne),
                )
              ]),
        ),
        Expanded(
          flex: 1,
          child: Padding(
            padding: EdgeInsets.symmetric(
                vertical: context.resources.dimens.semiTinyPadding),
            child: LinearProgressIndicator(
              value: card.progress,
              backgroundColor: context.resources.colors.drover,
              color: context.resources.colors.blueDianne,
            ),
          ),
        )
      ],
    );
  }
}
