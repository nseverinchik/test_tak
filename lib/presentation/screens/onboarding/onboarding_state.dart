class OnBoardingState {
  final String imageAsset;
  final String title;
  final String body;

  OnBoardingState({
    required this.body,
    required this.title,
    required this.imageAsset,
  });
}
