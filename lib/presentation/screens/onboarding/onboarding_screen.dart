import 'package:cardeo_test_task/presentation/utils/extensions/context_extension.dart';
import 'package:flutter/material.dart';

import 'onboarding_page.dart';
import 'onboarding_state.dart';

class OnBoardingScreen extends StatelessWidget {
  final controller = PageController(initialPage: 0);

  OnBoardingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        controller: controller,
        scrollDirection: Axis.horizontal,
        children: [
          OnBoardingPage(
            state: OnBoardingState(
                imageAsset: "assets/image/Icon3.png",
                title: "Save even more",
                body:
                    "Switch all your balances to Cardeo and\n you could save thousands every year"),
            backgroundColor: context.resources.colors.drover,
          ),
          OnBoardingPage(
            state: OnBoardingState(
                imageAsset: "assets/image/Icon1.png",
                title: "Repay, your way",
                body:
                    "Set a personalised repayment target\n and we'll help you stick to it"),
            backgroundColor: context.resources.colors.turquoise,
          ),
          OnBoardingPage(
            state: OnBoardingState(
                imageAsset: "assets/image/Icon2.png",
                title: "Personalised plans",
                body:
                    "We'll figure out the perfect\npayment plan for you - based on\n your personal financial situation"),
            backgroundColor: context.resources.colors.festival,
          )
        ],
      ),
    );
  }
}
