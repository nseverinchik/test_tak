import 'package:cardeo_test_task/presentation/utils/extensions/context_extension.dart';
import 'package:flutter/material.dart';

import 'onboarding_state.dart';

class OnBoardingPage extends StatelessWidget {
  final OnBoardingState state;
  final Color backgroundColor;

  const OnBoardingPage(
      {Key? key, required this.state, required this.backgroundColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: backgroundColor,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            children: [
              IconButton(
                  iconSize: 40,
                  onPressed: () {},
                  icon: const Icon(Icons.chevron_left)),
            ],
          ),
          Flexible(child: Image.asset(state.imageAsset)),
          Text(
            state.title,
            style: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: context.resources.dimens.semiHugeTextSize),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Center(
                child: Text(
              state.body,
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontWeight: FontWeight.w300,
                  fontSize: context.resources.dimens.semiSmallTextSize),
            )),
          ),
          ElevatedButton(
            onPressed: () {},
            child: Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 20.0, horizontal: 40),
              child: Text(
                context.resources.strings.getStarted,
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w600,
                    fontSize: context.resources.dimens.semiSmallTextSize),
              ),
            ),
            style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(
                    context.resources.colors.blueDianne),
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30)))),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                TextButton(
                    onPressed: () {},
                    child: Text(
                      context.resources.strings.logIn,
                      style:
                          TextStyle(color: context.resources.colors.blueDianne),
                    ))
              ],
            ),
          )
        ],
      ),
    );
  }
}
