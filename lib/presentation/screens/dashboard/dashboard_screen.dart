import 'package:cardeo_test_task/presentation/screens/dashboard/bloc/dashboard_cubit.dart';
import 'package:cardeo_test_task/presentation/screens/dashboard/bloc/dashboard_state.dart';
import 'package:cardeo_test_task/presentation/screens/dashboard/dashboard_screen_content.dart';
import 'package:cardeo_test_task/presentation/utils/di.dart';
import 'package:cardeo_test_task/presentation/utils/extensions/context_extension.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class DashboardScreen extends StatefulWidget {
  const DashboardScreen({Key? key}) : super(key: key);

  @override
  State<DashboardScreen> createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  final DashboardCubit cubit = DashboardCubit.empty(DI
      .getInstance()
      .dashboardUseCase);

  final isNotifyPayment = false;
  int selectedTabIndex = 0;

  @override
  void initState() {
    super.initState();
    cubit.fetchData();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DashboardCubit, DashboardState>(
        bloc: cubit,
        builder: (context, state) {
          return Scaffold(
            backgroundColor: context.resources.colors.alabaster,
            appBar: AppBar(
              backgroundColor: context.resources.colors.blueDianne,
              leading: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Container(
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(
                          width: 2,
                          color: Colors.white,
                          style: BorderStyle.solid)),
                  child: Center(child: Text(state.userInitials)),
                ),
              ),
              title: Text(
                context.resources.strings.dashboard,
                style: const TextStyle(color: Colors.white),
              ),
              actions: [
                IconButton(
                    onPressed: () {},
                    icon: const Icon(
                      Icons.view_headline,
                      color: Colors.white,
                    ))
              ],
            ),
            body: DashboardScreenContent(
              segments: state.values,
              value: state.summaryBalance,
            ),
            bottomNavigationBar: BottomNavigationBar(
              iconSize: 30,
              currentIndex: selectedTabIndex,
              onTap: (int index) {
                onTabClick(index);
              },
              selectedItemColor: context.resources.colors.blueDianne,
              showUnselectedLabels: true,
              unselectedItemColor: Colors.grey,
              items: [
                BottomNavigationBarItem(
                    icon: const Icon(
                      Icons.dashboard,
                      color: Colors.grey,
                    ),
                    label: context.resources.strings.dashboard),
                BottomNavigationBarItem(
                  icon: Stack(
                    children: [
                      const Icon(
                        Icons.payment,
                        color: Colors.grey,
                      ),
                      if (isNotifyPayment)
                        const Positioned(
                          // draw a red marble
                          top: 0.0,
                          right: 0.0,
                          child: Icon(Icons.brightness_1,
                              size: 10.0, color: Colors.redAccent),
                        )

                    ],
                  ),
                  label: context.resources.strings.paymentPlan,
                ),
                BottomNavigationBarItem(
                    icon: const Icon(
                      Icons.call_to_action_outlined,
                      color: Colors.grey,
                    ),
                    label: context.resources.strings.myDeals),
                BottomNavigationBarItem(
                    icon: const Icon(
                      Icons.support,
                      color: Colors.grey,
                    ),
                    label: context.resources.strings.support),
              ],
            ),
          );
        });
  }

  void onTabClick(int index) {
    setState(() {
      selectedTabIndex = index;
    });
  }
}
