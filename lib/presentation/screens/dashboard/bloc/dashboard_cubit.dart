import 'package:cardeo_test_task/domain/use_cases/get_use_case.dart';
import 'package:cardeo_test_task/presentation/screens/dashboard/bloc/dashboard_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class DashboardCubit extends Cubit<DashboardState>{
  final GetUseCase<DashboardState> getUseCase;

  DashboardCubit.empty(this.getUseCase) : super(DashboardState.defaultValue);

  DashboardCubit(DashboardState initialState,this.getUseCase) : super(initialState);

  void fetchData() {
    emit(getUseCase.get());
  }
}