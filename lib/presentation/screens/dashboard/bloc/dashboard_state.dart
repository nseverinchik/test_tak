import 'dart:math';
import 'dart:ui';

import 'package:cardeo_test_task/data/dto/card_dto.dart';
import 'package:flutter/material.dart';

class DashboardState {
  final String userInitials;
  final double summaryBalance;
  late final List<SegmentState> values;

  DashboardState(
      this.userInitials, this.summaryBalance, List<CardDTO> listCards) {
    values = listCards
        .map(
            (e) => SegmentState(getRandomColor(), ((1 / summaryBalance) * e.value)))
        .toList();
  }

  Color getRandomColor() =>
      Color((Random().nextDouble() * 0xFFFFFF).toInt()).withOpacity(1.0);

  static final DashboardState defaultValue = DashboardState("AS", 2000, [
    CardDTO.MOCK,
    CardDTO.MOCK,
    CardDTO.MOCK,
    CardDTO.MOCK,
  ]);
}

class SegmentState {
  final Color color;
  final double value;

  SegmentState(this.color, this.value);
}
