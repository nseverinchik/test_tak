import 'package:cardeo_test_task/data/dto/card_dto.dart';
import 'package:cardeo_test_task/presentation/basic/item_card.dart';
import 'package:cardeo_test_task/presentation/utils/extensions/context_extension.dart';
import 'package:flutter/material.dart';

class MediumCardWidget extends StatelessWidget {
  final CardDTO card;

  const MediumCardWidget({Key? key, required this.card}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Card(
        elevation: 2,
        child: SizedBox(
          height: 120,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(
                  height: 40,
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          height: 10,
                          width: 10,
                          decoration: BoxDecoration(
                              shape: BoxShape.circle, color: card.color),
                        ),
                        Container(
                          padding: const EdgeInsets.all(4),
                          decoration: BoxDecoration(
                              shape: BoxShape.rectangle,
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(10)),
                              color: context.resources.colors.drover),
                          child: Text(
                            context.resources.strings.overdueUppercase,
                            style: TextStyle(
                                color: context.resources.colors.blueDianne,
                                fontSize: 10,
                                fontWeight: FontWeight.w800),
                          ),
                        ),
                      ],
                    ),
                  )),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: ItemCard(card: card, cardHeight: 50),
              )
            ],
          ),
        ),
      ),
    );
  }
}
