import 'package:flutter/material.dart';

import 'dart:math' as math;

import 'bloc/dashboard_state.dart';

class SegmentedCurvedProgressIndicator extends StatefulWidget {
  final List<SegmentState> segments;
  final Color backgroundColor;
  final double width;
  final double height;
  final double strokeWidth;
  final Widget child;

  const SegmentedCurvedProgressIndicator(
      {required this.segments,
      required this.backgroundColor,
      required this.height,
      required this.width,
      required this.strokeWidth,
      required this.child,
      Key? key})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _SegmentedCurvedProgressState();
}

class _SegmentedCurvedProgressState
    extends State<SegmentedCurvedProgressIndicator>
    with SingleTickerProviderStateMixin {
  late Animation<double> animation;
  late AnimationController animationController;

  @override
  void initState() {
    super.initState();
    animationController =
        AnimationController(duration: const Duration(seconds: 2), vsync: this);
    final curvedAnimation = CurvedAnimation(
        parent: animationController, curve: Curves.easeInOutCubic);
    animation = Tween<double>(begin: 0.0, end: 3.14).animate(curvedAnimation)
      ..addListener(() {
        setState(() {});
      });
    animationController.forward();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: AlignmentDirectional.bottomCenter,
      children: [
        widget.child,
        CustomPaint(
          size: Size(widget.width, widget.height),
          painter: SegmentedProgressArcPainter(
              segments: widget.segments,
              arc: animation.value,
              isBackground: false,
              width: widget.width,
              height: widget.height,
              strokeWidth: widget.strokeWidth),
        ),
      ],
    );
  }
}

class SegmentedProgressArcPainter extends CustomPainter {
  List<SegmentState> segments;
  bool isBackground;
  double? arc;
  double width;
  double height;
  double strokeWidth;

  SegmentedProgressArcPainter(
      {this.arc,
      required this.segments,
      required this.isBackground,
      required this.width,
      required this.height,
      required this.strokeWidth});

  @override
  void paint(Canvas canvas, Size size) {
    final rect = Rect.fromLTRB(0, 0, width, height);
    var startAngle = -math.pi;
    const useCenter = false;

    if (isBackground) {
    } else {
      for (int i = 0; i < segments.length; i++) {
        final sweepAngle = math.pi * segments[i].value - 0.03;
        final paint = Paint()
          ..strokeCap = StrokeCap.butt
          ..color = segments[i].color
          ..style = PaintingStyle.stroke
          ..strokeWidth = strokeWidth;
        canvas.drawArc(rect, startAngle, sweepAngle, useCenter, paint);
        startAngle = startAngle + sweepAngle + 0.03;
      }
    }
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
