import 'package:cardeo_test_task/data/dto/card_dto.dart';
import 'package:cardeo_test_task/presentation/screens/dashboard/bloc/dashboard_state.dart';
import 'package:cardeo_test_task/presentation/screens/dashboard/segmented_circular_progress_indicator.dart';
import 'package:cardeo_test_task/presentation/utils/extensions/context_extension.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'medium_card_widger.dart';

class DashboardScreenContent extends StatelessWidget {
  final List<SegmentState> segments;
  final double value;

  const DashboardScreenContent(
      {Key? key,
      required this.value,
      required this.segments})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(slivers: [
      SliverList(
        delegate: SliverChildListDelegate([
          Padding(
            padding: const EdgeInsets.all(30),
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 20),
              decoration: const BoxDecoration(
                  shape: BoxShape.rectangle,
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Colors.grey),
              child: Column(
                children: [
                  Text(
                      "Pay your debts off more quickly with a Cardeo payment plan!",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: context.resources.colors.blueDianne,
                          fontWeight: FontWeight.w600,
                          fontSize:
                              context.resources.dimens.semiSmallTextSize)),
                  ElevatedButton(
                    onPressed: () {},
                    child: Text(
                      "Create my payment plan",
                      style: TextStyle(
                          color: context.resources.colors.blueDianne,
                          fontWeight: FontWeight.w600,
                          fontSize: context.resources.dimens.semiSmallTextSize),
                    ),
                    style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all(Colors.white),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(30)))),
                  )
                ],
              ),
            ),
          ),
          SizedBox(
            width: 200,
            height: 180,
            child: LayoutBuilder(
                builder: (BuildContext context, BoxConstraints constraints) {
              return SegmentedCurvedProgressIndicator(
                segments: segments,
                backgroundColor: context.resources.colors.drover,
                height: constraints.maxHeight * 3 / 2,
                width: constraints.maxWidth * 2 / 3,
                strokeWidth: constraints.maxHeight / 5,
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "£$value",
                        style: TextStyle(
                            fontSize: context.resources.dimens.semiHugeTextSize,
                            color: context.resources.colors.blueDianne,
                            fontWeight: FontWeight.w600),
                      ),
                      Text(
                        "on ${segments.length} cards",
                        style: TextStyle(
                            fontSize: context.resources.dimens.mediumTextSize,
                            color: context.resources.colors.blueDianne),
                      )
                    ]),
              );
            }),
          ),
          for (int i = 0; i < 12; i++)
            MediumCardWidget(
              card: CardDTO.MOCK,
            )
        ]),
      ),
    ]);
  }
}
