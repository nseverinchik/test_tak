import 'package:cardeo_test_task/presentation/utils/extensions/context_extension.dart';
import 'package:flutter/material.dart';

class AccountIcon extends StatelessWidget {
  const AccountIcon({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          width: 40,
          height: 40,
          decoration: BoxDecoration(
              shape: BoxShape.circle, color: context.resources.colors.festival),
        ), // TODO add image
        const Positioned(
            right: 0,
            top: 0,
            child: Icon(
              Icons.circle,
              color: Colors.red,
              size: 12,
            ))
      ],
    );
  }
}
