import 'package:cardeo_test_task/presentation/utils/extensions/context_extension.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import 'circular_progress_indicator.dart';

class TotalBalanceInfoWidget extends StatelessWidget {
  final double totalBalance;
  final String currency;
  final double progress;

  const TotalBalanceInfoWidget(
      {required this.progress,
      required this.currency,
      required this.totalBalance,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
      return CurvedProgressIndicator(
        progress: progress,
        backgroundColor: context.resources.colors.drover,
        valueColor: context.resources.colors.blueDianne,
        height: constraints.maxHeight * 3 / 2,
        width: constraints.maxWidth * 2 / 3,
        strokeWidth: constraints.maxHeight / 5,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            FlexibleFittedBox(
              flexFit: FlexFit.loose,
              boxFit: BoxFit.scaleDown,
              child: Text(
                context.resources.strings.totalBalance,
                style: TextStyle(
                    fontWeight: FontWeight.w300,
                    color: context.resources.colors.blueDianne),
              ),
            ),
            FlexibleFittedBox(
              flexFit: FlexFit.loose,
              boxFit: BoxFit.scaleDown,
              child: Text.rich(
                TextSpan(
                    text: currency,
                    style: TextStyle(
                        fontSize: context.resources.dimens.semiHugeTextSize),
                    children: <InlineSpan>[
                      TextSpan(
                        text: '$totalBalance',
                        style: TextStyle(
                            fontWeight: FontWeight.w800,
                            fontSize: context.resources.dimens.hugeTextSize,
                            color: context.resources.colors.blueDianne),
                      ),
                    ]),
              ),
            ),
            Flexible(
                child: RichText(
                    text: TextSpan(
                        text: context.resources.strings.makePayment,
                        style: const TextStyle(
                            color: Colors.grey, fontWeight: FontWeight.w400),
                        recognizer: TapGestureRecognizer()..onTap = () {})))
          ],
        ),
      );
    });
  }
}

class FlexibleFittedBox extends StatelessWidget {
  final Widget child;
  final FlexFit flexFit;
  final BoxFit boxFit;

  const FlexibleFittedBox(
      {required this.child,
      Key? key,
      required this.flexFit,
      required this.boxFit})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Flexible(fit: flexFit, child: FittedBox(fit: boxFit, child: child));
  }
}
