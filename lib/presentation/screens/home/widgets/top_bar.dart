import 'package:cardeo_test_task/presentation/utils/extensions/context_extension.dart';
import 'package:flutter/material.dart';

import 'account_icon.dart';

class TopBar extends StatelessWidget {
  final String name;

  const TopBar({required this.name, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children:  [
          const Padding(
            padding: EdgeInsets.all(5.0),
            child: Icon(
              Icons.settings,
              size: 30,
              color: Colors.black,
            ),
          ),
          Text(
            '${context.resources.strings.hello} $name',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: context.resources.dimens.semiSmallTextSize),
          ),
          const AccountIcon()
        ],
      ),
    );
  }
}
