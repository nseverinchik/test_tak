
import 'package:flutter/material.dart';

import 'dart:math' as math;

class CurvedProgressIndicator extends StatefulWidget {
  final double progress;
  final Color backgroundColor;
  final Color valueColor;
  final double width;
  final double height;
  final double strokeWidth;
  final Widget child;

  const CurvedProgressIndicator(
      {required this.progress,
        required this.backgroundColor,
        required this.valueColor,
        required this.height,
        required this.width,
        required this.strokeWidth,
        required this.child,
        Key? key})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _CurvedProgressState();
}

class _CurvedProgressState extends State<CurvedProgressIndicator>
    with SingleTickerProviderStateMixin {
  late Animation<double> animation;
  late AnimationController animationController;

  @override
  void initState() {
    super.initState();
    animationController =
        AnimationController(duration: const Duration(seconds: 2), vsync: this);
    final curvedAnimation = CurvedAnimation(
        parent: animationController, curve: Curves.easeInOutCubic);
    animation = Tween<double>(begin: 0.0, end: 3.14).animate(curvedAnimation)
      ..addListener(() {
        setState(() {});
      });
    animationController.forward();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: AlignmentDirectional.bottomCenter,
      children: [
        widget.child,
        CustomPaint(
          size: Size(widget.width, widget.height),
          painter: ProgressArcPainter(1, null, widget.backgroundColor, false,
              widget.width, widget.height, widget.strokeWidth),
        ),
        CustomPaint(
          size: Size(widget.width, widget.height),
          painter: ProgressArcPainter(
              widget.progress,
              animation.value,
              widget.valueColor,
              false,
              widget.width,
              widget.height,
              widget.strokeWidth),
        ),
      ],
    );
  }
}

class ProgressArcPainter extends CustomPainter {
  bool isBackground;
  double? arc;
  Color progressColor;
  double progress;
  double width;
  double height;
  double strokeWidth;

  ProgressArcPainter(this.progress, this.arc, this.progressColor,
      this.isBackground, this.width, this.height, this.strokeWidth);

  @override
  void paint(Canvas canvas, Size size) {
    final rect = Rect.fromLTRB(0, 0, width, height);
    const startAngle = -math.pi;
    final sweepAngle = arc ?? math.pi;
    const useCenter = false;
    final paint = Paint()
      ..strokeCap = StrokeCap.square
      ..color = progressColor
      ..style = PaintingStyle.stroke
      ..strokeWidth = strokeWidth;

    if (isBackground) {}
    canvas.drawArc(rect, startAngle, sweepAngle * progress, useCenter, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
