import 'package:cardeo_test_task/presentation/utils/extensions/context_extension.dart';
import 'package:flutter/material.dart';

class InfoCard extends StatelessWidget {
  final String title;
  final String body;
  final String assetsPath;

  const InfoCard(
      {Key? key,
      required this.title,
      required this.body,
      required this.assetsPath})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return VerticalInfoCardLayout(
        topWidget: SizedBox(
          height: 100,
          child: Image.asset(assetsPath),
        ),
        topFlex: 2,
        centerWidget: Text(
          title,
          textAlign: TextAlign.center,
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: context.resources.dimens.semiSmallTextSize),
        ),
        centerFlex: 1,
        bottomWidget: Text(
          body,
          maxLines: 2,
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: context.resources.dimens.smallTextSize),
          overflow: TextOverflow.ellipsis,
        ),
        bottomFlex: 1);
  }
}

class VerticalInfoCardLayout extends StatelessWidget {
  final Widget topWidget;
  final int topFlex;
  final Widget centerWidget;
  final int centerFlex;
  final Widget bottomWidget;
  final int bottomFlex;

  const VerticalInfoCardLayout({
    Key? key,
    required this.topWidget,
    required this.topFlex,
    required this.centerWidget,
    required this.centerFlex,
    required this.bottomWidget,
    required this.bottomFlex,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Flexible(flex: topFlex, fit: FlexFit.tight, child: topWidget),
            Flexible(flex: centerFlex, fit: FlexFit.tight, child: centerWidget),
            Flexible(flex: bottomFlex, fit: FlexFit.tight, child: bottomWidget)
          ],
        ),
      ),
    );
  }
}
