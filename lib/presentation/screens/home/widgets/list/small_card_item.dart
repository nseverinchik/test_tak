import 'package:cardeo_test_task/data/dto/card_dto.dart';
import 'package:cardeo_test_task/presentation/basic/item_card.dart';
import 'package:cardeo_test_task/presentation/utils/extensions/context_extension.dart';
import 'package:flutter/material.dart';

class SmallCardWidget extends StatelessWidget {
  final CardDTO card;

  const SmallCardWidget({Key? key, required this.card}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
        elevation: 2,
        shape: RoundedRectangleBorder(
            borderRadius:
                BorderRadius.circular(context.resources.dimens.mediumRadius)),
        child: Padding(
            padding: EdgeInsets.all(context.resources.dimens.smallPadding),
            child: ItemCard(
              card: card,
              cardHeight: 100,
            )));
  }
}
