import 'package:cardeo_test_task/presentation/screens/home/bloc/home_header_state.dart';
import 'package:cardeo_test_task/presentation/utils/extensions/context_extension.dart';
import 'package:flutter/material.dart';

import 'info_card_widget.dart';
import 'top_bar.dart';
import 'total_balance_info_widget.dart';

class Header extends SliverPersistentHeaderDelegate {
  final double headerExpandedHeight;
  final double headerCollapsedHeight;
  final int animationDurationInMills;
  final double fractionBackgroundHeight;

  final HeaderState state;

  Header(
      {required this.state,
      required this.headerExpandedHeight,
      required this.headerCollapsedHeight,
      required this.animationDurationInMills,
      required this.fractionBackgroundHeight});

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    final isExpanded =
        shrinkOffset > headerExpandedHeight - headerCollapsedHeight;

    final animationDuration = Duration(milliseconds: animationDurationInMills);

    return Stack(
      fit: StackFit.expand,
      children: [
        AnimatedOpacity(
            opacity: !isExpanded ? 1 : 0,
            duration: animationDuration,
            child: LayoutBuilder(
              builder: (context, constraints) {
                return SizedBox(
                    height: constraints.maxHeight,
                    child: ExpandedHeader(
                      state: state,
                      backgroundHeight:
                          constraints.maxHeight * fractionBackgroundHeight,
                      borderRadius: context.resources.dimens.bigRadius,
                    ));
              },
            )),
        AnimatedOpacity(
            opacity: isExpanded ? 1 : 0,
            duration: animationDuration,
            child: CollapsedHeader(
              borderRadius: context.resources.dimens.bigRadius,
              name: state.userName,
            ))
      ],
    );
  }

  @override
  double get maxExtent => headerExpandedHeight;

  @override
  double get minExtent => headerCollapsedHeight;

  @override
  bool shouldRebuild(covariant SliverPersistentHeaderDelegate oldDelegate) =>
      true;
}

class ExpandedHeader extends StatelessWidget {
  final double backgroundHeight;
  final double borderRadius;
  final HeaderState state;

  const ExpandedHeader(
      {Key? key,
      required this.backgroundHeight,
      required this.borderRadius,
      required this.state})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      Container(
        height: backgroundHeight,
        decoration: BoxDecoration(
            color: context.resources.colors.turquoise,
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(borderRadius),
                bottomRight: Radius.circular(borderRadius))),
      ),
      Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          TopBar(
            name: state.userName,
          ),
          Flexible(
              child: ExpandedHeaderContent(
            progress: state.progress,
            currency: state.currency,
            totalBalance: state.totalBalance,
          ))
        ],
      ),
    ]);
  }
}

class CollapsedHeader extends StatelessWidget {
  final String name;
  final double borderRadius;

  const CollapsedHeader(
      {Key? key, required this.name, required this.borderRadius})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: context.resources.colors.turquoise,
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(borderRadius),
              bottomRight: Radius.circular(borderRadius))),
      child: TopBar(
        name: name,
      ),
    );
  }
}

class ExpandedHeaderContent extends StatelessWidget {
  final double totalBalance;
  final String currency;
  final double progress;

  const ExpandedHeaderContent(
      {required this.progress,
      required this.currency,
      required this.totalBalance,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Flexible(
            flex: 2,
            child: TotalBalanceInfoWidget(
              totalBalance: totalBalance,
              currency: currency,
              progress: progress,
            )),
        Flexible(
          flex: 3,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              const Spacer(
                flex: 1,
              ),
              Flexible(
                flex: 10,
                child: InfoCard(
                    title: 'Repayment Calculator',
                    body: context.resources.strings.mock,
                    assetsPath: "assets/image/Icon1.png"),
              ),
              const Spacer(
                flex: 1,
              ),
              Flexible(
                flex: 10,
                child: InfoCard(
                    title: '£648.43',
                    body: context.resources.strings.mock,
                    assetsPath: "assets/image/Icon3.png"),
              ),
              const Spacer(
                flex: 1,
              )
            ],
          ),
        )
      ],
    );
  }
}
