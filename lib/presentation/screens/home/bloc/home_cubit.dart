import 'package:cardeo_test_task/domain/use_cases/get_use_case.dart';
import 'package:cardeo_test_task/presentation/screens/home/bloc/home_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomeCubit extends Cubit<HomeState> {
  final GetUseCase<HomeState> getUseCase ;
  HomeCubit.empty(this.getUseCase) : super(HomeState.defaultValue);

  HomeCubit(HomeState initialState,this.getUseCase) : super(initialState);

  void fetchData() {
    emit(getUseCase.get());
  }
}
