
import 'package:cardeo_test_task/data/dto/card_dto.dart';

import 'home_header_state.dart';

class HomeState{
  final HeaderState headerState;
  final List<CardDTO> listCards;

  const HomeState(this.headerState,this.listCards);

  static final HomeState defaultValue = HomeState(HeaderState("",0.0,'',0.0),List.empty());
}
