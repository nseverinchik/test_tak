
class HeaderState {
  final String userName;
  final double totalBalance;
  final String currency;
  final double progress;

  HeaderState(this.userName, this.totalBalance, this.currency, this.progress);
}