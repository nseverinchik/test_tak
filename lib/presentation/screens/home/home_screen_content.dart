import 'package:cardeo_test_task/presentation/screens/home/bloc/home_state.dart';
import 'package:cardeo_test_task/presentation/utils/Consts.dart';
import 'package:flutter/material.dart';

import 'widgets/header.dart';
import 'widgets/list/small_card_item.dart';
import 'widgets/list/sticky_header.dart';

class HomeScreenContent extends StatelessWidget {
  final HomeState state;

  const HomeScreenContent({required this.state, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final heightScreen = MediaQuery.of(context).size.height;
    final headerHeight = heightScreen * 0.7;
    const collapsedHeaderHeight = 100.0;

    return CustomScrollView(
      slivers: [
        SliverPersistentHeader(
          delegate: Header(
              state: state.headerState,
              headerExpandedHeight: headerHeight,
              headerCollapsedHeight: collapsedHeaderHeight,
              animationDurationInMills: Constants.minimalAnimationDurationInMills,
              fractionBackgroundHeight: 0.7),
          pinned: true,
        ),
        SliverList(
            delegate: SliverChildListDelegate([
          const CardsStickyHeader(),
          for (int i = 0; i < state.listCards.length; i++)
            Container(
              height: 100,
              padding: const EdgeInsets.all(10.0),
              child: SmallCardWidget(
                card: state.listCards[i],
              ),
            )
        ]))
      ],
    );
  }
}
