import 'package:cardeo_test_task/presentation/screens/home/bloc/home_cubit.dart';
import 'package:cardeo_test_task/presentation/screens/home/bloc/home_state.dart';
import 'package:cardeo_test_task/presentation/utils/di.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'home_screen_content.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final HomeCubit cubit = HomeCubit.empty(DI.getInstance().homeGetUseCase);

  @override
  void initState() {
    super.initState();
    cubit.fetchData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<HomeCubit, HomeState>(
        bloc: cubit,
        builder: (context, state) {
          return SafeArea(
              child: Container(
            color: Theme.of(context).backgroundColor,
            child: HomeScreenContent(
              state: state,
            ),
          ));
        },
      ),
    );
  }
}
