import 'package:cardeo_test_task/presentation/screens/home/home_screen.dart';
import 'package:cardeo_test_task/presentation/utils/di.dart';
import 'package:cardeo_test_task/presentation/utils/extensions/context_extension.dart';
import 'package:flutter/material.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: context.resources.colors.turquoise,
        backgroundColor: context.resources.colors.alabaster,
      ),
      home: FutureBuilder(
          future: DI.getInstance().init(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              return const HomeScreen();
            } else {
              return const CircularProgressIndicator();
            }
          }),
    );
  }
}
