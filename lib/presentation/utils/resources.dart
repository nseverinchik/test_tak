import 'package:cardeo_test_task/presentation/utils/colors.dart';
import 'package:cardeo_test_task/presentation/utils/strings.dart';
import 'package:flutter/material.dart';

import 'dimens.dart';

class Resources {
  BuildContext _context;

  Resources(this._context);

  AppColors get colors => AppColors();
  StringsResources get strings => StringsResources();
  Dimens get dimens => Dimens();

  static Resources from(BuildContext context) => Resources(context);
}
