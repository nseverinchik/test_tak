
import 'package:cardeo_test_task/presentation/utils/resources.dart';
import 'package:flutter/material.dart';

extension AppContext on BuildContext{
  Resources get resources => Resources.from(this);

}