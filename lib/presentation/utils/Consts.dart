class Constants {
  static const int minimalAnimationDurationInMills = 100;
  static const double iconItemCardSize = 60;
}
