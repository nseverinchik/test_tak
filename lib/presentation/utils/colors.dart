import 'dart:ui';

import 'package:cardeo_test_task/presentation/utils/extensions/color_extension.dart';

class AppColors {
  Color get turquoise => HexColor.fromHex("#44D0D0"); //header // slide one
  Color get blueDianne => HexColor.fromHex("#1E3A47"); // secondary
  Color get drover => HexColor.fromHex("#FCF8BB"); //tertiary //slide three
  Color get alabaster => HexColor.fromHex("#F8F8F8"); //background
  Color get festival => HexColor.fromHex("#FBF066"); // slide two
}
