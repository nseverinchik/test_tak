import 'package:cardeo_test_task/data/repositories/mock_cards_repository_impl.dart';
import 'package:cardeo_test_task/data/repositories/mock_user_repository_impl.dart';
import 'package:cardeo_test_task/domain/repositories/cards_repository.dart';
import 'package:cardeo_test_task/domain/repositories/user_repository.dart';
import 'package:cardeo_test_task/domain/use_cases/get_dashboard_data_use_case.dart';
import 'package:cardeo_test_task/domain/use_cases/get_home_data_use_case.dart';
import 'package:cardeo_test_task/domain/use_cases/get_use_case.dart';
import 'package:cardeo_test_task/presentation/screens/dashboard/bloc/dashboard_state.dart';
import 'package:cardeo_test_task/presentation/screens/home/bloc/home_state.dart';

class DI {
  static DI? instance;

  late CardsRepository cardsRepository;
  late UserRepository userRepository;
  late GetUseCase<HomeState> homeGetUseCase;
  late GetUseCase<DashboardState> dashboardUseCase;

  DI._();

  static DI getInstance() {
    return instance ?? (instance = DI._());
  }

  Future<void> init() async {
    cardsRepository = MockCardsRepositoryImpl();
    userRepository = MockUserRepositoryImpl();
    homeGetUseCase = GetHomeDataUseCase(
        cardsRepository: cardsRepository, userRepository: userRepository);
    dashboardUseCase = GetDashboardDataUseCase();
  }
}
