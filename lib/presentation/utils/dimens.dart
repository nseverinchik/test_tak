class Dimens {
  double get bigRadius => 40;

  double get mediumRadius => 15;

  double get semiTinyPadding => 8;

  double get smallPadding => 10;

  double get smallTextSize => 12;

  double get semiSmallTextSize => 16;

  double get mediumTextSize => 18;

  double get semiHugeTextSize => 32;

  double get hugeTextSize => 48;
}
