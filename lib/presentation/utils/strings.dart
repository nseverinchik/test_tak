class StringsResources{
  String get myCards =>'My cards';
  String get mock => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. ';
  String get makePayment => 'Make a payment →';
  String get totalBalance => 'Total balance';
  String get hello => 'Hello';
  String get dashboard => 'Dashboard';
  String get getStarted=>"Get started";
  String get logIn=>"Log in";
  String get overdueUppercase => "OVERDUE";
  String get paymentPlan => 'Payment Plan';
  String get myDeals => 'My Deals';
  String get support =>  'Support';
}