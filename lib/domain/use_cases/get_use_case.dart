abstract class GetUseCase<T> {
  T get();
}
