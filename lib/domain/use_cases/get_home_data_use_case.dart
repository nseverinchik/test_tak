import 'package:cardeo_test_task/domain/repositories/cards_repository.dart';
import 'package:cardeo_test_task/domain/repositories/user_repository.dart';
import 'package:cardeo_test_task/domain/use_cases/get_use_case.dart';
import 'package:cardeo_test_task/presentation/screens/home/bloc/home_header_state.dart';
import 'package:cardeo_test_task/presentation/screens/home/bloc/home_state.dart';

class GetHomeDataUseCase implements GetUseCase<HomeState> {
  final CardsRepository cardsRepository;

  final UserRepository userRepository;

  GetHomeDataUseCase(
      {required this.cardsRepository, required this.userRepository});

  @override
  HomeState get() {
    final user = userRepository.fetch();
    final cards = cardsRepository.fetch();
    final HeaderState headerState = HeaderState(
        user.name, user.currentBalance, user.balanceCurrency, user.progress);

    return HomeState(headerState, cards);
  }
}
