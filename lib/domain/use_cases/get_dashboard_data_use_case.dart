import 'package:cardeo_test_task/domain/use_cases/get_use_case.dart';
import 'package:cardeo_test_task/presentation/screens/dashboard/bloc/dashboard_state.dart';

class GetDashboardDataUseCase implements GetUseCase<DashboardState> {
  @override
  DashboardState get() => DashboardState.defaultValue;

}