import 'package:cardeo_test_task/data/dto/card_dto.dart';

abstract class CardsRepository {
   List<CardDTO> fetch();
}
