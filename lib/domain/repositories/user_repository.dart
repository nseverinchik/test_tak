
import 'package:cardeo_test_task/data/dto/user_dto.dart';

abstract class UserRepository{
  UserDTO fetch();
}