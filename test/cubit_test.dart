import 'package:bloc_test/bloc_test.dart';
import 'package:cardeo_test_task/domain/use_cases/get_use_case.dart';
import 'package:cardeo_test_task/presentation/screens/dashboard/bloc/dashboard_cubit.dart';
import 'package:cardeo_test_task/presentation/screens/dashboard/bloc/dashboard_state.dart';
import 'package:cardeo_test_task/presentation/screens/home/bloc/home_cubit.dart';
import 'package:cardeo_test_task/presentation/screens/home/bloc/home_state.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MockHomeCubit extends MockCubit<HomeState> implements HomeCubit {}
class MockDashboardCubit extends MockCubit<DashboardState> implements DashboardCubit {}

class FakeHomeState extends Fake implements HomeState {}
class FakeDashboardState extends Fake implements DashboardState {}

final mockHomeCubit = MockHomeCubit();
final mockDashboardCubit = MockDashboardCubit();

class MockGetHomeDataUseCase extends Mock implements GetUseCase<HomeState> {
  @override
  HomeState get() => HomeState.defaultValue;
}
class MockGetDashboardDataUseCase extends Mock implements GetUseCase<DashboardState> {
  @override
  DashboardState get() => DashboardState.defaultValue;
}

void main() {
  test('nothing emits in HomeCubit when nothing called', () {
    whenListen(
        mockHomeCubit,
        Stream.fromIterable([
          HomeState.defaultValue,
        ]));
    expectLater(mockHomeCubit.stream, emitsInOrder([]));
  });

  test('nothing emits in DashboardCubit when nothing called', () {
    whenListen(
        mockDashboardCubit,
        Stream.fromIterable([
          DashboardState.defaultValue,
        ]));
    expectLater(mockDashboardCubit.stream, emitsInOrder([]));
  });

  final mockGetHomeUseCase = MockGetHomeDataUseCase();

  blocTest<HomeCubit, HomeState>(
    'emits [] when nothing is called',
    build: () => HomeCubit.empty(mockGetHomeUseCase),
    act: (cubit) => cubit.fetchData(),
    expect: () => <HomeState>[HomeState.defaultValue],
  );

  final mockGetDashboardUseCase = MockGetDashboardDataUseCase();

  blocTest<DashboardCubit, DashboardState>(
    'emits [] when nothing is called',
    build: () => DashboardCubit.empty(mockGetDashboardUseCase),
    act: (cubit) => cubit.fetchData(),
    expect: () => <DashboardState>[DashboardState.defaultValue],
  );
}
