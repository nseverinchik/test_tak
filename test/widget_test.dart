
import 'package:cardeo_test_task/presentation/app.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('test', (WidgetTester tester) async {
    await tester.pumpWidget(const App());
    var widget = find.byType(MaterialApp);
    expect(widget, findsOneWidget);
  });
}
