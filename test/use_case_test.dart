import 'package:cardeo_test_task/domain/use_cases/get_use_case.dart';
import 'package:cardeo_test_task/presentation/screens/dashboard/bloc/dashboard_state.dart';
import 'package:cardeo_test_task/presentation/screens/home/bloc/home_state.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MockGetHomeUseCase extends Mock implements GetUseCase<HomeState> {
  @override
  HomeState get() => HomeState.defaultValue;
}

class MockGetDashboardUseCase extends Mock
    implements GetUseCase<DashboardState> {
  @override
  DashboardState get() => DashboardState.defaultValue;
}

void main() {
  test("GetHomeUseCase   test", () {
    final mockGetUseCase = MockGetHomeUseCase();
    var actual = mockGetUseCase.get();
    expect(actual, HomeState.defaultValue);
  });

  test("GetDashboardUseCase test", () {
    final mockGetUseCase = MockGetDashboardUseCase();
    var actual = mockGetUseCase.get();
    expect(actual, DashboardState.defaultValue);
  });
}
